const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	registerOrder,
	getAllOrders,
	checkOrder,
	profileOrder,
	updateOrder,
	updateCountOrder,
	activeOrder,
	notActiveOrder,
	deleteOrder
	
} = require('./../controllers/orderControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

// GENERAL: GET ALL ORDERS - OK
	router.get('/', async (req, res) => {

		try{
			await getAllOrders().then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: REGISTER ORDER - OK
	router.post('/register', async (req, res) => {

		try{
			await registerOrder(req, res).then(result => res.send(result))
		} catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: CHECK IF ORDER ALREADY EXISTS - OK
	router.post('/order-exists', async (req, res) => {
		try{
			await checkOrder(req.body).then(result => res.send(result))

		}catch(error){
			res.status(500).json(error)
		}
	})


// TOKEN: RETRIEVE ORDER INFORMATION - OK
	router.get('/order', verify, async (req, res) => {

		try{
			profileOrder(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: UPDATE ORDER ActiveStatus - OK
	router.put('/update-order', verify, async (req, res) => {
		try{
			await updateOrder(req.body).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	})


// ADMIN: DELETE ORDER - OK
	router.delete('/delete-order', verifyAdmin, async (req, res) => {
		try{
			deleteOrder(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


//Export the router module to be used in index.js file
module.exports = router;
