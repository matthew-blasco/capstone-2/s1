const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	registerProd,
	getAllProd,
	checkProd,
	profileProd,
	updateProd,
	updateCount,
	activeStatus,
	notActiveStatus,
	deleteProd
	
} = require('./../controllers/productController');

const {verify, decode, verifyAdmin} = require('./../auth');

// GENERAL: GET ALL PRODUCTS - OK
	router.get('/', async (req, res) => {

		try{
			await getAllProd().then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: REGISTER PRODUCT - OK
	router.post('/register', async (req, res) => {
		// console.log(req.body)	//user object

		try{
			await registerProd(req.body).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: CHECK IF PRODUCT ALREADY EXISTS - OK
	router.post('/product-exists', async (req, res) => {
		try{
			await checkProd(req.body).then(result => res.send(result))

		}catch(error){
			res.status(500).json(error)
		}
	})


// TOKEN: RETRIEVE PRODUCT INFORMATION - OK
	router.get('/product', verify, async (req, res) => {
		// console.log(req.headers.authorization)
		// const userId = decode(req.headers.authorization).id
		// console.log(userId)

		try{
			profileProd(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: UPDATE PRODUCT INFORMATION - OK
	router.put('/update-product', verify, async (req, res) => {
		// console.log( decode(req.headers.authorization).id )
		// const userId = decode(req.headers.authorization).id

		// console.log(req.body)
		try{
			await updateProd(req.body).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: UPDATE PRODUCT COUNT - OK
	router.patch('/update-count', verify, async (req, res) => {
		// console.log( decode(req.headers.authorization).id )
		// console.log(req.body.count)
		// console.log(req.body)
		try{
			await updateCount(req.body.productName, req.body.quantity).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})


// ADMIN: SET isActive: True - OK
	router.patch('/isActive', verifyAdmin, async (req, res) => {
		try{
			await activeStatus(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}

	})


// ADMIN: SET isActive: False - OK
	router.patch('/isNotActive', verifyAdmin, async (req, res) => {
		try{
			await notActiveStatus(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// ADMIN: DELETE PRODUCT - OK
	router.delete('/delete-product', verifyAdmin, async (req, res) => {
		try{
			deleteProd(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})




//Export the router module to be used in index.js file
module.exports = router;
