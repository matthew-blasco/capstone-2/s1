const express = require('express');
const router = express.Router();
const User = require('./../models/User');
const Order = require('./../models/Order');

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePw,
	adminStatus,
	userStatus,
	deleteUser,
	addToCart
	
} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

// GENERAL: GET ALL USERS - OK
	router.get('/', async (req, res) => {

		try{
			await getAllUsers().then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: REGISTER USER - OK
	router.post('/register', async (req, res) => {
		// console.log(req.body)	//user object

		try{
			await register(req.body).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})


// GENERAL: CHECK IF EMAIL ALREADY EXISTS - OK
	router.post('/email-exists', async (req, res) => {
		try{
			await checkEmail(req.body).then(result => res.send(result))

		}catch(error){
			res.status(500).json(error)
		}
	})


// GENERAL: LOGIN - OK
		// authentication
	router.post('/login', (req, res) => {
		// console.log(req.body)
		try{ 
			login(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: RETRIEVE USER INFORMATION - OK
	router.get('/profile', verify, async (req, res) => {
		// console.log(req.headers.authorization)
		const userId = decode(req.headers.authorization).id
		try{
			profile(userId).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: UPDATE USER INFORMATION - OK
	router.put('/update', verify, async (req, res) => {
		// console.log( decode(req.headers.authorization).id )

		const userId = decode(req.headers.authorization).id
		try{
			await update(userId, req.body).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	})


// TOKEN: UPDATE PASSWORD - OK 
	router.patch('/update-password', verify, async (req, res) => {
		console.log( decode(req.headers.authorization).id )
		console.log(req.body.password)

		const userId = decode(req.headers.authorization).id
		try{
			await updatePw(userId, req.body.password).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})


// ADMIN: SET isAdmin: True - OK
	router.patch('/isAdmin', verifyAdmin, async (req, res) => {
		try{
			await adminStatus(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}

	})


// ADMIN: SET isAdmin: False - OK
	router.patch('/isUser', verifyAdmin, async (req, res) => {
		try{
			await userStatus(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// ADMIN: DELETE USER - OK
	router.delete('/delete-user', verifyAdmin, async (req, res) => {
		try{
			deleteUser(req.body).then(result => res.send(result))

		}catch(err){
			res.status(500).json(err)
		}
	})


// ALL: ADD TO CART (OLD)
	// router.post('/add-to-cart', verify, async (req, res) => {
	// 	const user = decode(req.headers.authorization).isAdmin
		
	// 	const data ={
	// 		userId: req.body.userId,
	// 		productId: req.body.productId
	// 	}

	// 	if(user == false){
	// 		try{
	// 			await addToCart(data).then(result => res.send(result))

	// 		}catch(err){
	// 			res.status(500).json(err)
	// 		}

	// 	} else{
	// 		res.send(`Only with user access can add-to-cart`)
	// 	}	
	// })


// Add to Cart
	router.post("/cart", async (req, res) => {
	  const { userId, productId, quantity, name, price } = req.body

		try {	
		    let cartExists = await Order.findOne({ userId }).then(result => {
					return result
				});
		    let userExist = await User.findOne({ userId }).then(result => {
					return result
				});

		    if (cartExists) {
		      //Checkpoint
		      let itemExistsInCart = cartExists.products.findIndex(p => p.productId == productId)

			      if (itemExistsInCart > -1) {
			        //product exists in the cart, update the quantity
			        let productItem = cartExists.products[itemExistsInCart];
			        productItem.quantity = quantity;
			        cartExists.products[itemExistsInCart] = productItem;
			      	console.log("product quantity updated")
			      } else {
			        //product does not exists in cart, add new item
			        cartExists.products.push({ productId, quantity, name, price });
			        console.log("product pushed")
			      }
		      cartExists = await cartExists.save();
		      return res.status(201).send(cartExists);
		    } else {
		      //no cart for user, create new cart
		      const newCart = await Order.create({
		        userId,
		        products: [{ productId, quantity, name, price }]
		      });
		      console.log("new cart created")
		      let newIdOfNewCart = newCart.get("_id");
		      
		      //user's cart ID updated [Only have 1 cart at a time]
		      userExist.cart = newIdOfNewCart;
		      userExist = await userExist.save();
		      return res.status(201).send(newCart);
		    }
	  	} catch (err) {
	    console.log(err);
	    res.status(500).send("Something went wrong");
	  }
	});




//Export the router module to be used in index.js file
module.exports = router;
