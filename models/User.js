const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		default: "Guest_first_name",
		required: false
	},
	lastName: {
		type: String,
		default: "Guest_last_name",
		required: false
	},
	email: {
		type: String,
		required: [true, `email is required`],
		unique: true
	},
	password: {
		type: String,
		required: [true, `password is required`]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	isSeller: {
		type: Boolean,
		default: false
	},	
	cart: { type: mongoose.Schema.Types.ObjectId, ref: 'Order'}
}, {timestamps: true})


//Export the model
module.exports = mongoose.model(`User`, userSchema);
