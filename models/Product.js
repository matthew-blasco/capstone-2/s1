const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, `product name is required`]
    },
    category: {
        type: String,
        required: [true, `category is required`]
    },
    description: {
        type: String,
        required: [true, `product description is required`]
    },
    quantity: {
        type: Number,
        required: [true, `quantity is required`]
    },
    price: {
        type: Number,
        required: [true, `Price is required`]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    seller: {
        type: { type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    },
    buyers: [
        {
            userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
            pendingOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
}, {timestamps: true})

module.exports = mongoose.model("Product", productSchema);
