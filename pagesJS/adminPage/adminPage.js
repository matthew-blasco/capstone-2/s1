const log = console.log;
let sidebar = document.querySelector('.sidebar');
let sidebarBtn = document.querySelector('.sidebarBtn');
let general = document.getElementById('general');
let isAdmin = localStorage.getItem('admin');
let recentCarts = document.getElementById('recentSales');
let recentUsers = document.getElementById('allUsersTab');
let clickAllUsers = document.getElementById('seeAllUsers');
let clickDashboard = document.getElementById('sideDashboard'); 
	
	/*Nodes*/
		// General Page Modal
		var modal = document.getElementById("gpModal");
		var generalPageProceedButton = document.getElementById("gpmodalProceed");
		var generalPageCancelButton = document.getElementById("gpmodalCancel");

	/*Click Functions*/
		// Nav Bar: Dashboard Hamburger Button
			sidebarBtn.onclick = function() {
				sidebar.classList.toggle("active");
				if(sidebar.classList.contains("active")){
					sidebarBtn.classList.replace("bx-menu" ,"bx-menu-alt-right");
				}else
				sidebarBtn.classList.replace("bx-menu-alt-right", "bx-menu");
			}
		// Side Button: General Page Button
			general.onclick = function() {
				modal.style.display = "block";
					window.onclick = function(event) {
					  if (event.target == modal) {
					    modal.style.display = "none";
					  }
					}
					generalPageCancelButton.onclick = function (){
					    modal.style.display = "none";
					}
					generalPageProceedButton.onclick = function() {
						window.location.replace('./../general/general.html')
					}
			}
		// Side Button: See All Users 
			clickAllUsers.onclick = function() {
				if(isAdmin == 'true'){
					fetch(`http://localhost:5000/api/users/`)
					.then(result => result.json()).then(result => {
						courses = result.map(allusers => {
							console.log(allusers)
							const {
								_id,
								createdAt,
								firstName,
								lastName,
								email,
								password,
								isAdmin,
								isSeller
							} = allusers
						recentCarts.style.display = "none";
						recentUsers.style.display = "block";
						document.querySelector('.active').removeAttribute("class");
						clickAllUsers.setAttribute('class', 'active');
						//Listings
						const dateList = document.createElement("li");
						const emailList = document.createElement("li");
						const statusList = document.createElement("li");
						const creation = createdAt.slice(0, 10);
						const guestCreation = document.createTextNode(creation);
						const guestEmail = document.createTextNode(`${email}`);
						
						const checkStatus = (a, b) => {
							if(a == true && b == false) {
								return 'Seller'
							} else if(a == false && b == true){ 
								return 'Admin'
							} else { 
								return 'Guest'
							}
						}
						const guestStatusCheck = checkStatus(isSeller, isAdmin);
						const guestStatus = document.createTextNode(guestStatusCheck);

						dateList.appendChild(guestCreation);
						document.getElementById("auvDate").appendChild(dateList);

						emailList.appendChild(guestEmail);
						document.getElementById("auvEmail").appendChild(emailList);

						statusList.appendChild(guestStatus);
						document.getElementById("auvStatus").appendChild(statusList);
					})

					})
				} else {
					alert('You are not an admin')
				}
			}
		// Side Button: Dashboard
			clickDashboard.onclick = function() {

				if(isAdmin == 'true'){
					// fetch(`http://localhost:5000/api/users/`)
					// .then(result => result.json()).then(result => {
						recentCarts.style.display = "block";
						recentUsers.style.display = "none";
						document.querySelector('.active').removeAttribute("class");
						clickDashboard.setAttribute('class', 'active');

					// })
				} else {
					alert('You are not an admin')
				}
			}

