const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const User = require('./../models/User')
// const Course = require('./../models/Course')

//import createToken function from auth module
const {createToken} = require('./../auth');



//GET ALL USERS - OK
	module.exports.getAllUsers = async () => {

		return await User.find().then(result => result)
	}


//REGISTER A USER - OK
	module.exports.register = async (reqBody) => {
		// console.log(reqBody)
		const {email, password} = reqBody

		const newUser = new User({
			email: email,
			password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
			// password: bcrypt.hashSync(password, 10)
		})

		return await  newUser.save().then(result => {
			if(result){
				return true
			} else {
				if(result == null){
					return false
				}
			}
		})
	}


//CHECK IF EMAIL EXISTS - OK
	module.exports.checkEmail = async (reqBody) => {
		const {email} = reqBody

		return await User.findOne({email: email}).then((result, err) =>{
			if(result){
				return true
			} else {
				if(result == null){
					return false
				} else {
					return err
				}
			}
		})
	}


//LOGIN A USER - OK
	module.exports.login = async (reqBody) => {

		return await User.findOne({email: reqBody.email}).then((result, err) => {

			if(result == null){
				return {message: `User does not exist.`}

			} else {

				if(result !== null){
					//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
					const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

					console.log(reqBody.password == decryptedPw) //true
					

					if(reqBody.password == decryptedPw){
						//create a token for the user
						return { token: createToken(result) }
					} else {
						return {auth: `Auth Failed!`}
					}

				} else {
					return err
				}
			}
		})
	} 


//TOKEN: RETRIEVE USER INFORMATION - OK
	module.exports.profile = async (id) => {

		return await User.findById(id).then((result, err) => {
			if(result){
				return result
			}else{
				if(result == null){
					return {message: `user does not exist`}
				} else {
					return err
				}
			}
		})
	}


// UPDATE USER INFO - OK
	module.exports.update = async (userId, reqBody) => {
		const userData = {
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
		}

		return await User.findByIdAndUpdate(userId, {$set: userData}, {new:true}).then((result, err) => {
			// console.log(result)
			if(result){
				result.password = "***"
				return result
			} else{
				return err
			}
		})
	}


// UPDATE PASSWORD - OK
	module.exports.updatePw = async (id, password) => {
		let updatedPw = {
			password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		}

		return await User.findByIdAndUpdate(id, {$set: updatedPw})
		.then((result, err) => {
			if(result){
				result.password = "***"
				return result
			} else{ 
				return err
			}
		})
	}


//CHANGE TO ADMIN STATUS TO TRUE - OK
	module.exports.adminStatus = async (reqBody) => {
		const {email} = reqBody

		return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
	}


//CHANGE TO ADMIN STATUS TO FALSE - OK
	module.exports.userStatus = async (reqBody) => {

		return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
	}


//DELETE A USER - OK
	module.exports.deleteUser = async (reqBody) => {
		const {email} = reqBody
		return await User.findOneAndDelete({email: email}).then((result, err) => result ? true : err)
	}


// ADD TO CART (old)
		module.exports.addToCart = async (data) => {
			const {userId, productId} = data

			const updatedUser = await User.findById(userId).then(result => {
				result.cart.push({productId: productId})

				return result.save().then(user => user ? true : false)
			})



			const updatedCourse = await Product.findById(productId).then(result => {
				result.buyers.push({userId: userId})

				return result.save().then(course => course ? true : false)
			})


			if(updatedUser && updatedCourse){
				return true

			} else {
				false
			}


		}

// ADD TO CART (new)
		// module.exports.addToCart = async (data) => {
		// 	const { userId, productId, quantity, name, price } = data
		// 	//


		// 	let carts = await Order.findOne({ _id: userId }).then(result => {
		// 		return result
		// 	})

		// 	let user = await User.findOne({ _id: userId }).then(result => {
		// 		return result
		// 	});

		// 	const updatedUser = await User.findById(userId).then(result => {
		// 		result.cart.push({productId: productId})

		// 		return result.save().then(user => user ? true : false)
		// 	})



		// 	const updatedCourse = await Product.findById(productId).then(result => {
		// 		result.buyers.push({userId: userId})

		// 		return result.save().then(course => course ? true : false)
		// 	})


		// 	if(updatedUser && updatedCourse){
		// 		return true

		// 	} else {
		// 		false
		// 	}


		// }
