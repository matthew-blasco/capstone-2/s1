const CryptoJS = require("crypto-js");
const Order = require('./../models/Order')
const User = require('./../models/User')
const {createToken} = require('./../auth');



//GET ALL ORDERS - OK
	module.exports.getAllOrders = async () => {

		return await Order.find().then(result => result)
	}


//REGISTER ORDER - OK
	module.exports.registerOrder = async (req, res) => {
		const {
			userId,
			products
		} = req.body

		const newOrder = new Order({
			userId: userId
			
		})

		return await newOrder.save().then(result => {
			if(result){
				return true
			} else {
				if(result == null){
					return false
				}
			}
		})
	}


//CHECK IF ORDER EXISTS - OK
	module.exports.checkOrder = async (reqBody) => {
		const {
			userId
		} = reqBody

		return await Order.findOne({userId: userId}).then((result, err) =>{
			if(result){
				return true
			} else {
				if(result == null){
					return false
				} else {
					return err
				}
			}
		})
	}


//TOKEN: RETRIEVE ORDER INFORMATION - OK
	module.exports.profileOrder = async (reqBody) => {
		const {userId} = reqBody

		return await Order.findOne({userId: userId}).then((result, err) => {
			if(result){
				return result
			}else{
				if(result == null){
					return {message: `order does not exist`}
				} else {
					return err
				}
			}
		})
	}


// UPDATE ORDER ActiveStatus - OK
	module.exports.updateOrder = async (reqBody) => {
		const orderData = {active: reqBody.active}

		return await Order.findOneAndUpdate({userId: reqBody.userId}, {$set: orderData}, {new:true}).then((result, err) => {
			if(result){
				return result
			} else{
				return err
			}
		})
	}


//DELETE A USER - OK
	module.exports.deleteOrder = async (reqBody) => {
		const {userId} = reqBody
		return await Order.findOneAndDelete({userId: userId}).then((result, err) => result ? true : err)
	}