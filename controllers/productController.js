const CryptoJS = require("crypto-js");
// const bcrypt = require('bcrypt');
const Product = require('./../models/Product')
// const Course = require('./../models/Course')

//import createToken function from auth module
const {createToken} = require('./../auth');



//GET ALL PRODUCTS - OK
	module.exports.getAllProd = async () => {

		return await Product.find().then(result => result)
	}


//REGISTER PRODUCT - OK
	module.exports.registerProd = async (reqBody) => {
		// console.log(reqBody)
		const {
			productName,
			category,
			description,
			quantity,
			price,
			isActive,
			seller
		} = reqBody

		const newProd = new Product({
			productName: productName,
			category: category,
			description: description,
			quantity: quantity,
			price: price,
			isActive: isActive,
			seller: seller
			
		})

		return await  newProd.save().then(result => {
			if(result){
				return true
			} else {
				if(result == null){
					return false
				}
			}
		})
	}


//CHECK IF PRODUCT EXISTS - OK
	module.exports.checkProd = async (reqBody) => {
		const {productName} = reqBody

		return await Product.findOne({productName: productName}).then((result, err) =>{
			if(result){
				return true
			} else {
				if(result == null){
					return false
				} else {
					return err
				}
			}
		})
	}


//TOKEN: RETRIEVE PRODUCT INFORMATION - OK
	module.exports.profileProd = async (reqBody) => {
		const {productName} = reqBody

		return await Product.findOne({productName: productName}).then((result, err) => {
			if(result){
				return result
			}else{
				if(result == null){
					return {message: `product does not exist`}
				} else {
					return err
				}
			}
		})
	}


// UPDATE PRODUCT INFO - OK
	module.exports.updateProd = async (reqBody) => {
		const prodData = {
			productName: reqBody.productName,
			category: reqBody.category,
			description: reqBody.description,
			quantity: reqBody.quantity,
			price: reqBody.price,
			isActive: reqBody.isActive,
			seller: reqBody.seller
		}

		return await Product.findOneAndUpdate({productName: reqBody.productName}, {$set: prodData}, {new:true}).then((result, err) => {
			// console.log(result)
			if(result){
				return result
			} else{
				return err
			}
		})
	}


// UPDATE PRODUCT COUNT - OK - VALUES EXPRESSION FROM FRONTEND
	module.exports.updateCount = async (prodName, prodCount) => {

		
		let updatedCount = {quantity: prodCount}
	

		return await Product.findOneAndUpdate(prodName, {$set: updatedCount}, {new:true})
		.then((result, err) => {
			if(result){
				return result
			} else{ 
				return err
			}
		})
	}


//CHANGE TO ACTIVE STATUS TO TRUE - OK
	module.exports.activeStatus = async (reqBody) => {
		const {productName} = reqBody

		return await Product.findOneAndUpdate({productName: productName}, {$set: {isActive: true}}, {new:true}).then((result, err) => result ? true : err)
	}


//CHANGE TO ADMIN STATUS TO FALSE - OK
	module.exports.notActiveStatus = async (reqBody) => {
		const {productName} = reqBody

		return await Product.findOneAndUpdate({productName: productName}, {$set: {isActive: false}}).then((result, err) => result ? true : err)
	}


//DELETE A USER - OK
	module.exports.deleteProd = async (reqBody) => {
		const {productName} = reqBody
		return await Product.findOneAndDelete({productName: productName}).then((result, err) => result ? true : err)
	}

