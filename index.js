//INDEX.HTML Functions: 
//1. Login
//2. Signup
//3. Forgot Password


//1. Login
	const loginFile = document.getElementById('loginform')

	loginFile.addEventListener('submit', (e) => {
		e.preventDefault()

		let email = document.getElementById('logemail').value
		let pw = document.getElementById('logpass').value
		let preset1 = () => {loginFile.reset();}

		if(email){
				//send the request (including the user input) to the server
				fetch(`http://localhost:5000/api/users/login`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: pw
					})
				})
				//wait for the server's response
				.then(result => result.json())
				.then(result => {
					

					//once token is received, store it in the local storage
					if(result.message !== "User does not exist."){
						//store token in the local storage
						localStorage.setItem('token', result.token)

						// request for user information & store admin and id in the local storage
						let token = localStorage.getItem('token')
						fetch(`http://localhost:5000/api/users/profile`, {
							method: "GET",
							headers:{
								"Authorization": `Bearer ${token}`
							}
						})
						//wait for server's response
						.then(result => result.json())
						.then(result => {

							if(result.auth !== "auth failed!"){
								console.log(result)
								//store id & isAdmin to local storage
								localStorage.setItem('id', result._id)
								localStorage.setItem('admin', result.isAdmin)
								localStorage.setItem('email', result.email)
								localStorage.setItem('cart', result.cart.length)
								//notify the user successful login
								alert('Login successfully.')
								preset1()
								//redirect to courses page
								window.location.replace('./pagesHTML/general/general.html')
							} else {
								alert('Password incorrect, please check again.')
							}
						})

					} else {
						preset1()
						alert('User does not exist. Please try again.')

					}
				})

			} else {
				alert('Please input email & password!')
			}
		})


//2. Signup -- OK! 
	const signupForm = document.getElementById('signupform');
	signupForm.addEventListener("submit", (f) => {
		f.preventDefault()	

		const email2 = document.getElementById('signemail').value;
		const pw1 = document.getElementById('signpass1').value;
		const pw2 = document.getElementById('signpass2').value;
		const preset2 = () => {signupForm.reset();}
				

		if(pw1 === pw2){
			fetch(`http://localhost:5000/api/users/email-exists`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email2,
					password: pw1
				})
			})
			.then(result => result.json())
			.then(result => {
				if(result == false){
					fetch(`http://localhost:5000/api/users/register`, {
						method: "POST",
						headers:{
							"Content-Type": "application/json",
						},
						body: JSON.stringify({
							//user input
							email: email2,
							password: pw1
						})
					})
					.then(result => result.json())
					.then(result => {
						if(result){
							alert('User successfully registered!')
							let presets = () => {
									loginForm.style.marginLeft = "0%";
									loginText.style.marginLeft = "0%";
									signupSlider.checked = true;
								}
								presets()
							alert('Please login to continue!')
							
						} else {
							alert(`An unexpected error occured. Please try again`)
						}
					})

				} else {
					alert(`User already exists!`)
				}
			})
		} else {
			alert(`Confirm password does not match. Please check carefully.`)
		}

	
	});



//3. Forgot Password


